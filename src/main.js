import express from 'express'
import bodyParser from 'body-parser'
import axios from 'axios'
import { isArray, every, isString } from 'lodash'

const axiosInstance = axios.create({
    baseURL: 'http://' + (process.env.API || '127.0.0.1:11567'),
    timeout: 10000
})

const app = express()
app.use(bodyParser.json())

const get = function (url, response) {
    axiosInstance
        .get(url)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            console.log(error)
            response.sendStatus(500)
        })
}

const post = function (url, postData, response) {
    axiosInstance
        .post(url, postData)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            console.log(error)
            response.sendStatus(500)
        })
}

app.post('/api/unspentoutputs', (request, response) => {
    const addresses = request.body

    if (isArray(addresses) && every(addresses, isString)) {
        post('/addressdb/outputs', {
            'addresses': addresses,
            'mode': 'unspentOnly'
        }, response)
    } else {
        response.status(400).send('array of addresses is expected')
    }
})

app.post('/api/history', (request,response) => {
    const addresses = request.body

    if (isArray(addresses) && every(addresses, isString)) {
        post('/addressdb/transactions', {
            'addresses': addresses
        }, response)
    }
    else {
        response.status(400).send('array of addresses is expected')
    }
})

app.post('/api/publishtransaction', (request,response) => {
    const {tx} = request.body

    if (tx && isString(tx)) {
        post('/blockchain/publishtransaction', {tx})
    } else {
        response.status(400).send('expecting a tx field with base16 string')
    }
})

app.post('/api/runcontract', (request, response) => {
    const {body} = request

    if (!body.address) {
        response.status(400).send('address is missing')
        return
    }

    if (!body.tx) {
        response.status(400).send('tx is missing')
        return
    }

    // command, messageBody and options are optionable

    // returns a hex of transaction ready for signing
    response.json('00000000010126f78419f7dd51361cba21e272776ce435d2273dc1db358ecb38cd87cff56dd6000202203dd07717834caa760c499b0363da00fb91701e2be65d882db62f4ba158f6786b80c59702cbb376b1ec097ba870eaa6e7eb304bc5889b7dcc7fa96e1fe3ef193f240c0102204829b61f13a2ce594a1552e51ec4f52c405078a6b5ff692aa70f1f8aeb4862fa80c59702cbb376b1ec097ba870eaa6e7eb304bc5889b7dcc7fa96e1fe3ef193f240c0e0001016201038e49dce8661de84b6160c07fcfaef41659aff45b4934131a588a4d14fa15e68f9d2df4fa3dcbde177cce08934bc3cd4f3e242d13f93f3881a5a87145023b76ad645f13bbcfeb1b4331ad87433f2462443157569f1a356d104752295a339352c7')
})

app.get('/api/activecontracts', (request,response) => {
    get('/contract/active', response)
})

app.get('/api/info', (request,response) => {
    get('/blockchain/info', response)
})

app.listen(3000)